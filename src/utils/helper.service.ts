import { Injectable } from '@angular/core';
import { Product, SpendContributor } from '../models';

import { sum, map } from 'lodash';

@Injectable()
export class HelperService {
    shortId(): string {
        return ((Math.random() * Math.pow(36, 4) << 0).toString(36)).slice(-6);
    }

    spend(products: Product[]): number {
        return sum(map(products, x => +x.cost));
    }

    generateResult(people: SpendContributor[]): Wire[] {
        const result = people.map(function (man) {
            man.diff = man.own - man.spend;
            return man;
        });

        result.sort(function (man1, man2) {
            if (man1.diff > man2.diff) {
                return 1;
            }

            if (man1.diff < man2.diff) {
                return -1;
            }

            return 0;
        });

        const length = result.length - 1;
        const wires = [];

        for (let i = 0; i <= length; i++) {
            while (result[i].diff < -1) {
                for (let t = length; t > i; t--) {
                    if (result[t].diff > 0) {
                        const wireTitle = `${result[t].name} -> ${result[i].name}`;
                        const wireDiff = result[i].diff + result[t].diff;
                        let wireSum = 0;

                        if (wireDiff > 0) {
                            wireSum = Math.abs(result[i].diff);
                        } else {
                            wireSum = result[t].diff;
                        }

                        const wireObj = {
                            wireTitle,
                            wireSum
                        };

                        wires.push(wireObj);

                        result[i].diff += wireSum;
                        result[t].diff -= wireSum;
                    }
                };
            }
        };

        return wires;
    }
}

class Wire {
    wireTitle: string;
    wireSum: number;
}