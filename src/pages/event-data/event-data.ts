import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';

import { ContributorsPage } from '../contributors/contributors';

import { HelperService } from '../../utils/helper.service';

@Component({
    selector: 'event-data',
    templateUrl: 'event-data.html'
})
export class EventDataPage {
    eventData: EventData = new EventData();
    storageMarker: string = 'partyCalcEvents';

    constructor(public navCtrl: NavController, 
                public storage: Storage,
                private helper: HelperService) { }

    saveAndContinue() {
        let ed = this.eventData;
        let id = this.helper.shortId();

        let data = Object.assign({ id }, ed);

        this.storage.get(this.storageMarker)
            .then((list) => {
                if (!list) list = [];
                this.storage.set(this.storageMarker, [...list, data])

                return this.continue(id);
            });
    }

    continue(id: string) {
        let eventId = id || this.helper.shortId();
        this.navCtrl.push(ContributorsPage, { eventId });
    }

    
}

class EventData {
    title: string;
    date: Date;
};