import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { Contributor } from '../../models';
import { HelperService } from '../../utils/helper.service';
import { remove, find } from 'lodash';

import { AddCostsModalPage } from '../modals/add-costs/add-costs';
import { ProductsPage } from '../products/products';

@Component({
    selector: 'contributors',
    templateUrl: 'contributors.html'
})
export class ContributorsPage {
    currentEvent: CurrentEvent;
    newContributorName: string;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        private helper: HelperService) {

        this.currentEvent = new CurrentEvent(navParams.get('eventId'));
    }

    createContributor() {
        let name = this.newContributorName;

        if (!name) return false;

        this.newContributorName = "";
        this.currentEvent.addContributor(new Contributor(name));
    }

    removeContributor(id: string) {
        if (!id) return false;

        this.currentEvent.removeContributor(id);
    }

    editContributor(id: string) {
        if (!id) return false;

        let [deletedCont] = this.currentEvent.removeContributor(id);

        this.newContributorName = deletedCont.name;
    }

    removeProduct(contributorId: string, productId: string) {
        let contributor = find(this.currentEvent.contributors, { id: contributorId });

        if (contributor)
            contributor.removeProduct(productId);
    }

    addCostsTo(id: string) {
        let costsModal = this.modalCtrl.create(AddCostsModalPage, { contributorId: id });

        costsModal.onDidDismiss((data) => {
            if (!data) return false;
            let contributor = find(this.currentEvent.contributors, { id: data.id });

            if (contributor)
                contributor.products = contributor.products.concat(data.products);
        });

        costsModal.present();
    }

    totalSpend(contributor: Contributor) {
        return this.helper.spend(contributor.products);
    }

    nextStep() {
        this.navCtrl.push(ProductsPage, { event: this.currentEvent });
    }
}

class CurrentEvent {
    id: string;
    contributors: Contributor[];

    constructor(id: string) {
        this.id = id;
        this.contributors = [];
    }

    addContributor(contributor: Contributor) {
        this.contributors.push(contributor);
    }

    removeContributor(id: string) {
        return remove(this.contributors, { id: id });
    }
}
