import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

import { SpendContributor } from '../../../models';

import { filter } from 'lodash';

@Component({
    selector: 'select-contributors',
    templateUrl: 'select-contributors.html'
})
export class SelectContributorsModalPage {
    allContributors: SpendContributor[];

    constructor(public viewCtrl: ViewController, public params: NavParams) {
        this.allContributors = params.get('contributors');
    }

    dismiss(needSave: boolean) {
        let selected = null;

        if (needSave)
            selected = filter(this.allContributors, c => c.selected);

        this.viewCtrl.dismiss(selected);
    }
}