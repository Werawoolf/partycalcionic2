import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

import { Contributor, Product } from '../../../models';

@Component({
    selector: 'add-costs',
    templateUrl: 'add-costs.html'
})
export class AddCostsModalPage {
    newProductTitle: string;
    newProductPrice: number;
    contributor: CostsModalContributor;

    constructor(public viewCtrl: ViewController, public params: NavParams) {
        this.contributor = new CostsModalContributor(params.get('contributorId'));
    }

    createProduct() {
        let name = this.newProductTitle;
        let price = this.newProductPrice;

        if (!name || !price) return false;

        this.newProductTitle = "";
        this.newProductPrice = null;

        this.contributor.addProduct(new Product(name, price));
    }

    removeProduct(id: string) {
        if (!id) return false;

        this.contributor.removeProduct(id);
    }

    editProduct(id: string) {
        if (!id) return false;

        let [deletedProd] = this.contributor.removeProduct(id);

        this.newProductTitle = deletedProd.title;
        this.newProductPrice = deletedProd.cost;
    }

    dismiss(needSave: boolean) {
        this.viewCtrl.dismiss(needSave && this.contributor);
    }
}


class CostsModalContributor extends Contributor {
    constructor(id: string) {
        super("");

        this.id = id;
    }
 }