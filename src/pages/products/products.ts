import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { HelperService } from '../../utils/helper.service';

import { SpendContributor, Product } from '../../models';
import { map, find, filter, remove } from 'lodash';

import { SelectContributorsModalPage } from '../modals/select-contributors/select-contributors';
import { SummaryPage } from '../summary/summary';

@Component({
    selector: 'products',
    templateUrl: 'products.html'
})
export class ProductsPage {
    currentEvent: any;
    contributors: SpendContributor[];
    products: Product[];

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        private helper: HelperService) {

        this.currentEvent = navParams.get('event');

        this.products = [];
        this.contributors = map(this.currentEvent.contributors, c => {
            this.products = this.products.concat(c.products);
            let spend = this.helper.spend(c.products);

            return new SpendContributor(c.name, spend);
        });
    }

    addContributorTo(product: Product) {
        //select selected earlier
        let contributors = map(this.contributors, c => {
            c = Object.assign({}, c);

            let found = find(product.contributors, { id: c.id });
            if (found)
                c.selected = true;

            return c;
        });

        let contributorsModal = this.modalCtrl.create(SelectContributorsModalPage, { contributors });

        contributorsModal.onDidDismiss((data) => {
            if (!data || !data.length) return false;

            product.contributors = [].concat(data);
        });

        contributorsModal.present();
    }

    removeContributor(product: Product, contributor: SpendContributor) {
        remove(product.contributors, { id: contributor.id });
    }

    countOwn() {
        //set 0 for all
        map(this.contributors, c => {
            c.own = 0;
        });

        //count for every product
        map(this.products, p => {
            let part = +p.cost / p.contributors.length;

            map(p.contributors, c => {
                let contributor = find(this.contributors, { id: c.id });
                contributor.own += part;
            });
        });
    }

    everyProductHasContributors() {
        let len = this.products.length;

        return len && filter(this.products, p => p.contributors.length).length == len;
    }

    getResult() {
        this.countOwn();

        let products = this.products;
        let contributors = this.contributors;

        let result = this.helper.generateResult(this.contributors);

        this.navCtrl.push(SummaryPage, { result, products, contributors });
    }
}
