import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';

@Component({
    selector: 'summary',
    templateUrl: 'summary.html'
})
export class SummaryPage {
    result: any;
    products: any;
    contributors: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, ) {
        this.result = navParams.get('result');
        this.products = navParams.get('products');
        this.contributors = navParams.get('contributors');
    }

    close() {
         this.navCtrl.setRoot(HomePage);
    }
}
