import { NgModule, ErrorHandler } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { EventDataPage } from '../pages/event-data/event-data';
import { ContributorsPage } from '../pages/contributors/contributors';
import { AddCostsModalPage } from '../pages/modals/add-costs/add-costs';
import { ProductsPage } from '../pages/products/products';
import { SelectContributorsModalPage } from '../pages/modals/select-contributors/select-contributors';
import { SummaryPage } from "../pages/summary/summary";

import { HelperService } from '../utils/helper.service';

const pages = [
  HomePage,
  EventDataPage,
  ContributorsPage,
  AddCostsModalPage,
  ProductsPage,
  SelectContributorsModalPage,
  SummaryPage
];

@NgModule({
  declarations: [
    MyApp,
    ...pages
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ...pages
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler }, Storage, HelperService]
})
export class AppModule { }
