import { remove } from 'lodash';

import  { Product } from './product';
import { Entity } from './entity';

export class Contributor extends Entity {
    name: string;
    products: Product[];

    constructor(name: string) {
        super();

        this.name = name;
        this.products = [];
    }

    addProduct(product: Product) {
        this.products.push(product);
    }

    removeProduct(id: string) {
        return remove(this.products, { id: id });
    }
}