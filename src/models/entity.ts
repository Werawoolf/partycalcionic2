import { HelperService } from '../utils/helper.service';

const helper = new HelperService();

export class Entity {
    public id: string;

    constructor() {
        this.id = helper.shortId();    
    }
}