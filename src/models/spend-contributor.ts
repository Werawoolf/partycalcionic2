import { Selectable } from './selectable';
import { HelperService } from '../utils/helper.service';

const helper = new HelperService();

export class SpendContributor extends Selectable {
    id: string;
    name: string;
    own: number;
    spend: number;
    diff: number;

    constructor(name: string, spend: number) {
        super();

        this.id = helper.shortId();
        this.name = name;
        this.spend = spend;
        this.own = 0;
        this.diff = 0;
    }
}