import { Entity } from './entity';
import { SpendContributor } from './spend-contributor';

export class Product extends Entity {
    title: string;
    cost: number;
    contributors: SpendContributor[];

    constructor(title: string, price: number) {
        super();

        this.title = title;
        this.cost = price;
        this.contributors = [];
    }
}